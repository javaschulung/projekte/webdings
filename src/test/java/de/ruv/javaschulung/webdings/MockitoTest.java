package de.ruv.javaschulung.webdings;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mock;

import static org.mockito.Mockito.*;


public class MockitoTest {

	List<String> globalListe = mock(List.class);
	
	@Mock 
	List<String> globalListe2;
	
	
	@Test
	@Disabled
	public void test1() {
		
		List<String> mockedList = mock(List.class);
		
		mockedList.add("One");
		mockedList.clear();
		
		verify(mockedList).add("One");
		verify(mockedList).clear();
		
		LinkedList<String> mockedLinkedList = mock(LinkedList.class);
		
		when(mockedLinkedList.get(0)).thenReturn("first");
		
		System.out.println(mockedLinkedList.get(0));
		
		when(mockedLinkedList.get(1)).thenThrow(new RuntimeException());
		
		//System.out.println(mockedLinkedList.get(1));
		
		System.out.println(mockedLinkedList.get(999));
		
		// Aufruf der Liste mit irgendeinem Wert
		when(mockedLinkedList.get(anyInt())).thenReturn("Element");
		System.out.println(mockedLinkedList.get(42));
		verify(mockedLinkedList,times(3)).get(anyInt());
		
		// Exception werfen, wenn clear aufgerufen wird (bei einer Methode ohne Rückgabe/void)
		doThrow(new RuntimeException("Das darfst du nicht!")).when(mockedLinkedList).clear();
		mockedLinkedList.clear();
		
	}
	
	@Test
	public void test2() {
		
		List<String> singleMock = mock(List.class);
		
		singleMock.add("one");
		singleMock.add("two");
		
		// Prüfen der Reihenfolge der Aufrufe
		InOrder inOrder1 = inOrder(singleMock);
		inOrder1.verify(singleMock).add("one");
		inOrder1.verify(singleMock).add("two");
		
		
		// Prüfen der Reihenfolge der Aufrufe bei zwei Mocks
		List<String> firstMock = mock(List.class);
		List<String> secondMock = mock(List.class);
		
		firstMock.add("one");
		secondMock.add("two");
		
		InOrder inOrder2 = inOrder(firstMock,secondMock);
		inOrder2.verify(firstMock).add("one");
		inOrder2.verify(secondMock).add("two");
		
	}
	
	@Test
	public void test3() {	
		List<String> singleMock = mock(List.class);
		
		// Prüfen, dass niemals das add aufgerufen wird
		verify(singleMock, never()).add(any());
		
		
		// Prüfen, dass niemals mit diesem Mock gearbeitet wird
		verifyNoInteractions(singleMock);
			
	}
	
	
	@Test
	@Disabled
	public void test4(@Mock List<String> liste) {	
		
	
	}
	
	@Test
	public void test5() {	
		List<String> liste = spy(new ArrayList()); // "echtes" Objekt wird erzeugt
		when(liste.size()).thenReturn(100);
		
		System.out.println(liste.size()); 
		liste.add("one");
		System.out.println(liste.size()); 
		verify(liste).add("one");
	
	}	
}
