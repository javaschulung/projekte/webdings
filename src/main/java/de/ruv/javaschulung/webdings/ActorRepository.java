package de.ruv.javaschulung.webdings;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import de.ruv.javaschulung.webdings.model.Actor;

/*public interface ActorRepository extends CrudRepository<Actor, Integer> {

}
*/

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer> {
	List<Actor> findByLastName(String lastName);
	
	@Query ("SELECT a FROM Actor a WHERE a.lastName = ?1 AND a.firstName = ?2")
	List<Actor> findByLastName_FirstName(String lastName, String firstName);
	
	@Query ("SELECT a FROM Actor a WHERE a.lastName = ?1")
	List<Actor> findByLastName2(String lastName);
}