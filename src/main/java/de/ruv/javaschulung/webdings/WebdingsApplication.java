package de.ruv.javaschulung.webdings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebdingsApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebdingsApplication.class, args);
	}


}
