package de.ruv.javaschulung.webdings;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import de.ruv.javaschulung.webdings.model.Actor;

@RestController
public class ActorController {

	private final ActorRepository actorRepository;
	
	
	ActorController (ActorRepository actorRepository) {
		this.actorRepository = actorRepository;
	}
	
	
	@GetMapping("/actors")
	public List<Actor> getActors() {
		
		return actorRepository.findAll();
	}
	
	@PostMapping("/actors")
	public Actor newActor(@RequestBody Actor actor) {
		
		return actorRepository.save(actor);
	}
	
	@GetMapping("/actors/{id}")
	public Actor getActor(@PathVariable int id) {
		
		return actorRepository.findById(id)
				.orElseThrow( ()-> new ActorNotFoundException(id));
	}	
	
	
	@PutMapping("/actors/{id}")
	public Actor replaceActor(@PathVariable int id, @RequestBody Actor actor) {
		
		return actorRepository.findById(id)
			.map(ac -> {
				ac.setFirstName(actor.getFirstName());
				ac.setLastName(actor.getLastName());
				return actorRepository.save(ac);
			})
			.orElseGet(() -> {
				//actor.setId(id);  // durch autoincrement in der DB wird die ID durch die DB gesetzt, auch wenn hier etwas angegeben wurde.
				return actorRepository.save(actor);
			});
		
	}
	
	@DeleteMapping("/actors/{id}")
	public void deleteActor(@PathVariable int id) {
		
		actorRepository.deleteById(id);
		
	}
	
	
}
