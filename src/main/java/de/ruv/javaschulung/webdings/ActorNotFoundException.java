package de.ruv.javaschulung.webdings;

public class ActorNotFoundException extends RuntimeException {
	
	ActorNotFoundException(int id) {
		super("Konnte den Actor nicht finden " + id );
	}

}
