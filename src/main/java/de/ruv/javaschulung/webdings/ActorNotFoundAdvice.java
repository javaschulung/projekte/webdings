package de.ruv.javaschulung.webdings;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ControllerAdvice
public class ActorNotFoundAdvice {

	@ResponseBody
	@ExceptionHandler(ActorNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String actorNotFoundHandler(ActorNotFoundException ex) {
		return ex.getMessage();
	}
	
}
